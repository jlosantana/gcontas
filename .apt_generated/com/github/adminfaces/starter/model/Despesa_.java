package com.github.adminfaces.starter.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Despesa.class)
public abstract class Despesa_ {

	public static volatile SingularAttribute<Despesa, Tipo> tipo;
	public static volatile SingularAttribute<Despesa, String> cnes;
	public static volatile SingularAttribute<Despesa, Double> valor;
	public static volatile SingularAttribute<Despesa, Integer> id;
	public static volatile SingularAttribute<Despesa, Integer> version;
	public static volatile SingularAttribute<Despesa, String> competencia;

}

