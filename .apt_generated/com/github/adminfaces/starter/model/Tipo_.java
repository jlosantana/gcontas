package com.github.adminfaces.starter.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tipo.class)
public abstract class Tipo_ {

	public static volatile SingularAttribute<Tipo, String> name;
	public static volatile SingularAttribute<Tipo, Integer> id;
	public static volatile SingularAttribute<Tipo, Integer> version;

}

