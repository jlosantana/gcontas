/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.service;

import static com.github.adminfaces.template.util.Assert.has;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.deltaspike.data.api.criteria.Criteria;

import com.github.adminfaces.persistence.model.Filter;
import com.github.adminfaces.persistence.service.CrudService;
import com.github.adminfaces.starter.model.Despesa;
import com.github.adminfaces.starter.model.Despesa_;
import com.github.adminfaces.template.exception.BusinessException;

/**
 * @author rmpestano
 */
@Stateless
public class DespesaService extends CrudService<Despesa, Integer> implements Serializable {

	@Inject
	protected DespesaRepository despesaRepository;// you can create repositories to extract complex queries from your service

	protected Criteria<Despesa, Despesa> configRestrictions(Filter<Despesa> filter) {

		Criteria<Despesa, Despesa> criteria = criteria();

		// create restrictions based on parameters map
		if (filter.hasParam("id")) {
			criteria.eq(Despesa_.id, filter.getIntParam("id"));
		}

		if (filter.hasParam("minValor") && filter.hasParam("maxValor")) {
			criteria.between(Despesa_.valor, filter.getDoubleParam("minValor"), filter.getDoubleParam("maxValor"));
		} else if (filter.hasParam("minValor")) {
			criteria.gtOrEq(Despesa_.valor, filter.getDoubleParam("minValor"));
		} else if (filter.hasParam("maxValor")) {
			criteria.ltOrEq(Despesa_.valor, filter.getDoubleParam("maxValor"));
		}

		// create restrictions based on filter entity
		if (has(filter.getEntity())) {
			Despesa filterEntity = filter.getEntity();
			if (has(filterEntity.getCnes())) {
				criteria.likeIgnoreCase(Despesa_.cnes, "%" + filterEntity.getCnes());
			}

			if (has(filterEntity.getValor())) {
				criteria.eq(Despesa_.valor, filterEntity.getValor());
			}

			if (has(filterEntity.getCompetencia())) {
				criteria.likeIgnoreCase(Despesa_.competencia, "%" + filterEntity.getCompetencia() + "%");
			}
		}
		return criteria;
	}

	public void insert(List<Despesa> despesas) {
		despesas.forEach(desp -> {
			try {
				getEntityManager().persist(desp);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	public void beforeInsert(Despesa despesa) {
		validate(despesa);
	}

	public void beforeUpdate(Despesa despesa) {
		validate(despesa);
	}

	public void validate(Despesa despesa) {
		BusinessException be = new BusinessException();
		if (!despesa.hasCnes()) {
			be.addException(new BusinessException("CNES não pode ser vazio."));
		}

		if (!despesa.hasCompetencia()) {
			be.addException(new BusinessException("Competência não pode ser vazio."));
		} else {
			DateFormat format = new SimpleDateFormat("MM/yyyy");
			format.setLenient(false);
			try {
				format.parse(despesa.getCompetencia());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				be.addException(new BusinessException("Competência com formáto inválido."));
			}
		}

		if (!has(despesa.getValor())) {
			be.addException(new BusinessException("Valor não pode ser vazio."));
		}

		if (count(criteria().eqIgnoreCase(Despesa_.cnes, despesa.getCnes()).eqIgnoreCase(Despesa_.competencia, despesa.getCompetencia())
				.notEq(Despesa_.id, despesa.getId())) > 0) {

			be.addException(new BusinessException("Competência deve ser único."));
		}

		if (has(be.getExceptionList())) {
			throw be;
		}
	}

	public List<Despesa> listByCnes(String cnes) {
		return criteria().likeIgnoreCase(Despesa_.cnes, cnes).getResultList();
	}

	public List<String> getCness(String query) {
		return criteria().select(String.class, attribute(Despesa_.cnes)).likeIgnoreCase(Despesa_.cnes, "%" + query + "%").getResultList();
	}

	public Double getTotalValorByCnes(Despesa despesa) {
		if (!has(despesa.hasCnes())) {
			throw new BusinessException("CNES deve ser informado.");
		}
		return despesaRepository.getTotalValorByCnes(despesa.getCnes().toUpperCase());
	}

}
