/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.service;

import static com.github.adminfaces.template.util.Assert.has;

import java.io.Serializable;

import javax.ejb.Stateless;

import org.apache.deltaspike.data.api.criteria.Criteria;

import com.github.adminfaces.persistence.model.Filter;
import com.github.adminfaces.persistence.service.CrudService;
import com.github.adminfaces.starter.model.Tipo;
import com.github.adminfaces.starter.model.Tipo_;
import com.github.adminfaces.template.exception.BusinessException;

/**
 * @author rmpestano
 */
@Stateless
public class TipoService extends CrudService<Tipo, Integer> implements Serializable {

	protected Criteria<Tipo, Tipo> configRestrictions(Filter<Tipo> filter) {

		Criteria<Tipo, Tipo> criteria = criteria();

		// create restrictions based on parameters map
		if (filter.hasParam("id")) {
			criteria.eq(Tipo_.id, filter.getIntParam("id"));
		}

		// create restrictions based on filter entity
		if (has(filter.getEntity())) {
			Tipo filterEntity = filter.getEntity();
			if (has(filterEntity.getName())) {
				criteria.likeIgnoreCase(Tipo_.name, "%" + filterEntity.getName() + "%");
			}
		}
		return criteria;
	}

	public void beforeInsert(Tipo tipo) {
		validate(tipo);
	}

	public void beforeUpdate(Tipo tipo) {
		validate(tipo);
	}

	public void validate(Tipo tipo) {
		BusinessException be = new BusinessException();
		if (!tipo.hasName()) {
			be.addException(new BusinessException("Nome deve ser informado."));
		}
		if (count(criteria().eqIgnoreCase(Tipo_.name, tipo.getName()).notEq(Tipo_.id, tipo.getId())) > 0) {
			be.addException(new BusinessException("Nome deve ser único."));
		}
		if (has(be.getExceptionList())) {
			throw be;
		}
	}

	public Tipo findByNome(String nome) {
		return criteria().eq(Tipo_.name, nome).getSingleResult();
	}

}
