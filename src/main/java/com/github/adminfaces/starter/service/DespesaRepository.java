package com.github.adminfaces.starter.service;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.Repository;

import com.github.adminfaces.starter.model.Despesa;

@Repository
public interface DespesaRepository extends EntityRepository<Despesa, Integer> {

	@Query("SELECT SUM(c.valor) FROM Despesa c WHERE upper(c.cnes) like :cnes")
	Double getTotalValorByCnes(@QueryParam("cnes") String cnes);

}
