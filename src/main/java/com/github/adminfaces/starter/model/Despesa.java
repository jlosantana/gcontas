/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.github.adminfaces.persistence.model.BaseEntity;

/**
 * @author rmpestano
 */
@Entity
@Table(name = "despesa")
public class Despesa extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "cnes")
	private String cnes;

	@Column(name = "competencia")
	private String competencia;

	@Column(name = "valor")
	private Double valor;

	@ManyToOne
	@JoinColumn(name = "tipo_id")
	private Tipo tipo;

	@Version
	private Integer version;

	public Despesa() {
	}

	public Despesa(Integer id) {
		this.id = id;
	}

	public String getCnes() {
		return cnes;
	}

	public Double getValor() {
		return valor;
	}

	@Override
	public Integer getId() {
		return id;
	}

	public String getCompetencia() {
		return competencia;
	}

	public void setCnes(String cnes) {
		this.cnes = cnes;
	}

	public void setCompetencia(String competencia) {
		this.competencia = competencia;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Despesa cnes(String cnes) {
		this.cnes = cnes;
		return this;
	}

	public Despesa valor(Double valor) {
		this.valor = valor;
		return this;
	}

	public Despesa competencia(String competencia) {
		this.competencia = competencia;
		return this;
	}

	public boolean hasCnes() {
		return cnes != null && !"".equals(cnes.trim());
	}

	public boolean hasCompetencia() {
		return competencia != null && !"".equals(competencia.trim());
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
}
