/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import com.github.adminfaces.persistence.model.BaseEntity;

/**
 * @author rmpestano
 */
@Entity
@Table(name = "tipo")
public class Tipo extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "name")
	private String name;

	@Version
	private Integer version;

	public Tipo() {
	}

	public Tipo(Integer id) {
		this.id = id;
	}

	@Override
	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Tipo name(String name) {
		this.name = name;
		return this;
	}

	public boolean hasName() {
		return name != null && !"".equals(name.trim());
	}
}
