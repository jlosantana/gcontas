package com.github.adminfaces.starter.bean;

import static com.github.adminfaces.persistence.util.Messages.addDetailMessage;
import static com.github.adminfaces.template.util.Assert.has;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;

import com.github.adminfaces.persistence.bean.CrudMB;
import com.github.adminfaces.persistence.service.CrudService;
import com.github.adminfaces.persistence.service.Service;
import com.github.adminfaces.starter.model.Tipo;
import com.github.adminfaces.starter.service.TipoService;
import com.github.adminfaces.template.exception.BusinessException;

/**
 * Created by rmpestano on 12/02/17.
 */
@Named
@ViewScoped
public class TipoListMB extends CrudMB<Tipo> implements Serializable {

	@Inject
	TipoService tipoService;

	@Inject
	@Service
	CrudService<Tipo, Integer> crudService; // generic injection

	@Inject
	public void initService() {
		setCrudService(tipoService);
	}

	public void findTipoById(Integer id) {
		if (id == null) {
			throw new BusinessException("Provide Tipo ID to load");
		}
		Tipo tipoFound = crudService.findById(id);
		if (tipoFound == null) {
			throw new BusinessException(String.format("No tipo found with id %s", id));
		}
		selectionList.add(tipoFound);
		getFilter().addParam("id", id);
	}

	public void delete() {
		int numTipos = 0;
		for (Tipo selectedTipo : selectionList) {
			numTipos++;
			tipoService.remove(selectedTipo);
		}
		selectionList.clear();
		addDetailMessage(numTipos + " tipos deleted successfully!");
		clear();
	}

	public String getSearchCriteria() {
		StringBuilder sb = new StringBuilder(21);

		String nameParam = null;
		Tipo tipoFilter = filter.getEntity();

		Integer idParam = null;
		if (filter.hasParam("id")) {
			idParam = filter.getIntParam("id");
		}

		if (has(idParam)) {
			sb.append("<b>id</b>: ").append(idParam).append(", ");
		}

		if (filter.hasParam("name")) {
			nameParam = filter.getStringParam("name");
		} else if (has(tipoFilter) && tipoFilter.getName() != null) {
			nameParam = tipoFilter.getName();
		}

		if (has(nameParam)) {
			sb.append("<b>name</b>: ").append(nameParam).append(", ");
		}

		int commaIndex = sb.lastIndexOf(",");

		if (commaIndex != -1) {
			sb.deleteCharAt(commaIndex);
		}

		if (sb.toString().trim().isEmpty()) {
			return "No search criteria";
		}

		return sb.toString();
	}

}
