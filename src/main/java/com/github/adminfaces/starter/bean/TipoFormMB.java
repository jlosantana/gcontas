/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.bean;

import java.io.IOException;
import java.io.Serializable;

import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;

import com.github.adminfaces.persistence.bean.BeanService;
import com.github.adminfaces.persistence.bean.CrudMB;
import com.github.adminfaces.starter.model.Tipo;
import com.github.adminfaces.starter.service.TipoService;

/**
 * @author rmpestano
 */
@Named
@ViewScoped
@BeanService(TipoService.class) // use annotation instead of setter
public class TipoFormMB extends CrudMB<Tipo> implements Serializable {

	public void afterRemove() {
		try {
			addDetailMsg("Tipo " + entity.getName() + " removido com sucesso.");
			Faces.redirect("tipo-list.xhtml");
			clear();
			sessionFilter.clear(TipoListMB.class.getName());// removes filter saved in session for TipoListMB.

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void afterInsert() {
		addDetailMsg("Tipo " + entity.getName() + " cadastrado com sucesso.");
	}

	@Override
	public void afterUpdate() {
		addDetailMsg("Tipo " + entity.getName() + " updated successfully");
	}

}
