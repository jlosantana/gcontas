/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.adminfaces.starter.bean;

import java.io.IOException;
import java.io.Serializable;

import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;

import com.github.adminfaces.persistence.bean.BeanService;
import com.github.adminfaces.persistence.bean.CrudMB;
import com.github.adminfaces.starter.model.Despesa;
import com.github.adminfaces.starter.service.DespesaService;

/**
 * @author rmpestano
 */
@Named
@ViewScoped
@BeanService(DespesaService.class)//use annotation instead of setter
public class DespesaFormMB extends CrudMB<Despesa> implements Serializable {


    public void afterRemove() {
        try {
            addDetailMsg("Despesa " + entity.getCnes()
                    + " removido com sucesso.");
            Faces.redirect("despesa-list.xhtml");
            clear(); 
            sessionFilter.clear(DespesaListMB.class.getName());//removes filter saved in session for DespesaListMB.
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void afterInsert() {
         addDetailMsg("Despesa " + entity.getCnes() + " cadastrado com sucesso.");
    }

    @Override
    public void afterUpdate() {
        addDetailMsg("Despesa " + entity.getCnes() + " updated successfully");
    }
    

}
