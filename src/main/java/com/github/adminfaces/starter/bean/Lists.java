package com.github.adminfaces.starter.bean;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import com.github.adminfaces.persistence.service.CrudService;
import com.github.adminfaces.persistence.service.Service;
import com.github.adminfaces.starter.model.Despesa;
import com.github.adminfaces.starter.model.Despesa_;
import com.github.adminfaces.starter.model.Tipo;
import com.github.adminfaces.starter.service.TipoService;

@ApplicationScoped
public class Lists implements Serializable {

	@Inject
	@Service
	private CrudService<Despesa, Integer> crudService;

	@Inject
	private TipoService tipoService;

	@Produces
	@Named("cnes")
	public List<String> cnes() {
		return crudService.criteria().select(String.class, crudService.attribute(Despesa_.cnes)).getResultList();
	}

	@Produces
	@Named("tipos")
	public List<Tipo> tipos() {
		return tipoService.criteria().getResultList();
	}
}
