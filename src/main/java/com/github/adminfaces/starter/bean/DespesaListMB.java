package com.github.adminfaces.starter.bean;

import static com.github.adminfaces.persistence.util.Messages.addDetailMessage;
import static com.github.adminfaces.persistence.util.Messages.getMessage;
import static com.github.adminfaces.template.util.Assert.has;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.github.adminfaces.persistence.bean.CrudMB;
import com.github.adminfaces.persistence.model.Filter;
import com.github.adminfaces.persistence.service.CrudService;
import com.github.adminfaces.persistence.service.Service;
import com.github.adminfaces.starter.model.Despesa;
import com.github.adminfaces.starter.model.Tipo;
import com.github.adminfaces.starter.service.DespesaService;
import com.github.adminfaces.starter.service.TipoService;
import com.github.adminfaces.template.exception.BusinessException;

/**
 * Created by rmpestano on 12/02/17.
 */
@Named
@ViewScoped
public class DespesaListMB extends CrudMB<Despesa> implements Serializable {

	@Inject
	DespesaService despesaService;
	
	@Inject
	private TipoService tipoService;

	@Inject
	@Service
	CrudService<Despesa, Integer> crudService; // generic injection

	private UploadedFile file;

	@Inject
	public void initService() {
		setCrudService(despesaService);
	}
	
	@Override
	protected Filter<Despesa> initFilter() {
		Filter<Despesa> filter = super.initFilter();
		filter.getEntity().setTipo(new Tipo());
		return filter;
	}

	public void handleFileUpload(FileUploadEvent event) {
		try {
			Scanner scanner = new Scanner(event.getFile().getInputstream());
			Scanner dataScanner = null;
			int index = 0;
			List<Despesa> despList = new ArrayList<>();
			
			while (scanner.hasNextLine()) {
				dataScanner = new Scanner(scanner.nextLine());
				dataScanner.useDelimiter(";");
				Despesa desp = new Despesa();

				while (dataScanner.hasNext()) {
					String data = dataScanner.next();
					if (index == 0)
						desp.setCompetencia(new SimpleDateFormat("MM/yyyy").format(new SimpleDateFormat("yyyyMM").parse(data)));
					else if (index == 1)
						desp.setCnes(data);
					else if (index == 2) {
						Tipo tipo = null;
						try {
							tipo = tipoService.findByNome(data);
						} catch (Exception e) {
						}
						if (tipo == null) {
							tipo = new Tipo();
							tipo.setName(data);
							try {
								tipoService.saveOrUpdate(tipo);
							} catch (Exception e) {
								// TODO: handle exception
								continue;
							}
							if (tipo.getId() == null) {
								continue;
							}
							desp.setTipo(tipo);
						}
					}
					else if (index == 3)
						desp.setValor(new DecimalFormat("###,###,##0.00").parse(data).doubleValue());
					else
						System.out.println("invalid data::" + data);
					index++;
				}
				index = 0;
				despList.add(desp);
			}
			scanner.close();
			despesaService.insert(despList);
			FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public List<String> completeCnes(String query) {
		List<String> result = despesaService.getCness(query);
		return result;
	}

	public void findDespesaById(Integer id) {
		if (id == null) {
			throw new BusinessException("Provide Despesa ID to load");
		}
		Despesa despesaFound = crudService.findById(id);
		if (despesaFound == null) {
			throw new BusinessException(String.format("No despesa found with id %s", id));
		}
		selectionList.add(despesaFound);
		getFilter().addParam("id", id);
	}

	public void delete() {
		int numDespesas = 0;
		for (Despesa selectedDespesa : selectionList) {
			numDespesas++;
			despesaService.remove(selectedDespesa);
		}
		selectionList.clear();
		addDetailMessage(numDespesas + " despesas deleted successfully!");
		clear();
	}

	public String getSearchCriteria() {
		StringBuilder sb = new StringBuilder(21);

		String competenciaParam = null;
		Despesa despesaFilter = filter.getEntity();

		Integer idParam = null;
		if (filter.hasParam("id")) {
			idParam = filter.getIntParam("id");
		}

		if (has(idParam)) {
			sb.append("<b>id</b>: ").append(idParam).append(", ");
		}

		if (filter.hasParam("competencia")) {
			competenciaParam = filter.getStringParam("competencia");
		} else if (has(despesaFilter) && despesaFilter.getCompetencia() != null) {
			competenciaParam = despesaFilter.getCompetencia();
		}

		if (has(competenciaParam)) {
			sb.append("<b>competencia</b>: ").append(competenciaParam).append(", ");
		}

		String cnesParam = null;
		if (filter.hasParam("cnes")) {
			cnesParam = filter.getStringParam("cnes");
		} else if (has(despesaFilter) && despesaFilter.getCnes() != null) {
			cnesParam = despesaFilter.getCnes();
		}

		if (has(cnesParam)) {
			sb.append("<b>cnes</b>: ").append(cnesParam).append(", ");
		}

		Double valorParam = null;
		if (filter.hasParam("valor")) {
			valorParam = filter.getDoubleParam("valor");
		} else if (has(despesaFilter) && despesaFilter.getCnes() != null) {
			valorParam = despesaFilter.getValor();
		}

		if (has(valorParam)) {
			sb.append("<b>valor</b>: ").append(valorParam).append(", ");
		}

		if (filter.hasParam("minValor")) {
			sb.append("<b>").append(getMessage("label.minValor")).append("</b>: ").append(filter.getParam("minValor")).append(", ");
		}

		if (filter.hasParam("maxValor")) {
			sb.append("<b>").append(getMessage("label.maxValor")).append("</b>: ").append(filter.getParam("maxValor")).append(", ");
		}

		int commaIndex = sb.lastIndexOf(",");

		if (commaIndex != -1) {
			sb.deleteCharAt(commaIndex);
		}

		if (sb.toString().trim().isEmpty()) {
			return "No search criteria";
		}

		return sb.toString();
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

}
