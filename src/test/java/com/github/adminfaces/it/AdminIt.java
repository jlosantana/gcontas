package com.github.adminfaces.it;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.Cleanup;
import org.jboss.arquillian.persistence.TestExecutionPhase;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.MavenResolverSystem;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.github.adminfaces.persistence.model.AdminSort;
import com.github.adminfaces.persistence.model.Filter;
import com.github.adminfaces.persistence.service.CrudService;
import com.github.adminfaces.persistence.service.Service;
import com.github.adminfaces.starter.model.Despesa;
import com.github.adminfaces.starter.model.Despesa_;
import com.github.adminfaces.starter.service.DespesaService;
import com.github.adminfaces.template.exception.BusinessException;
import com.github.adminfaces.util.Deployments;

@RunWith(Arquillian.class)
public class AdminIt {

	@Deployment(name = "gcontas-it.war", testable = true)
	public static WebArchive createDeployment() {
		WebArchive war = Deployments.createDeployment();
		MavenResolverSystem resolver = Maven.resolver();
		war.addAsLibraries(resolver.loadPomFromFile("pom.xml").resolve("org.assertj:assertj-core").withTransitivity().asFile());
		System.out.println(war.toString(true));
		return war;
	}

	@Inject
	DespesaService despesaService;

	@Inject
	@Service
	CrudService<Despesa, Integer> crudService;

	@Test
	@UsingDataSet("despesas.yml")
	public void shouldCountDespesas() {
		assertEquals(despesaService.count().intValue(), 4);
	}

	@Test
	@UsingDataSet("despesas.yml")
	public void shouldFindDespesaById() {
		Despesa despesa = despesaService.findById(1);
		assertThat(despesa).isNotNull().extracting("id").contains(new Integer(1));
	}

	@Test
	@UsingDataSet("despesas.yml")
	public void shouldFindDespesaByExample() {
		Despesa despesaExample = new Despesa().cnes("Ferrari");
		List<Despesa> despesas = despesaService.example(despesaExample, Despesa_.cnes).getResultList();
		assertThat(despesas).isNotNull().hasSize(1).extracting("id").contains(new Integer(1));
	}

	@Test
	@Cleanup(phase = TestExecutionPhase.BEFORE)
	public void shouldNotInsertDespesaWithoutName() {
		long countBefore = despesaService.count();
		assertEquals(countBefore, 0);
		Despesa newDespesa = new Despesa().cnes("My Despesa").valor(1d);
		try {
			despesaService.insert(newDespesa);
		} catch (BusinessException e) {
			assertEquals("Despesa competencia cannot be empty", e.getExceptionList().get(0).getMessage());
		}
	}

	@Test
	public void shouldNotInsertDespesaWithoutCnes() {
		Despesa newDespesa = new Despesa().competencia("My Despesa").valor(1d);
		try {
			despesaService.insert(newDespesa);
		} catch (BusinessException e) {
			assertEquals("Despesa cnes cannot be empty", e.getExceptionList().get(0).getMessage());
		}
	}

	@Test
	@UsingDataSet("despesas.yml")
	@Cleanup(phase = TestExecutionPhase.BEFORE)
	public void shouldNotInsertDespesaWithDuplicateName() {
		Despesa newDespesa = new Despesa().cnes("My Despesa").competencia("ferrari spider").valor(1d);
		try {
			despesaService.insert(newDespesa);
		} catch (BusinessException e) {
			assertEquals("Despesa competencia must be unique", e.getExceptionList().get(0).getMessage());
		}
	}

	@Test
	public void shouldInsertDespesa() {
		long countBefore = despesaService.count();
		assertEquals(countBefore, 0);
		Despesa newDespesa = new Despesa().cnes("My Despesa").competencia("despesa competencia").valor(1d);
		despesaService.insert(newDespesa);
		assertEquals(countBefore + 1, despesaService.count().intValue());
	}

	@Test
	@UsingDataSet("despesas.yml")
	@Transactional(TransactionMode.DISABLED)
	public void shouldRemoveDespesa() {
		assertEquals(despesaService.count(despesaService.criteria().eq(Despesa_.id, 1)).intValue(), 1);
		Despesa despesa = despesaService.findById(1);
		assertNotNull(despesa);
		despesaService.remove(despesa);
		assertEquals(despesaService.count(despesaService.criteria().eq(Despesa_.id, 1)).intValue(), 0);
	}

	@Test
	@UsingDataSet("despesas.yml")
	public void shouldListDespesasCnes() {
		List<Despesa> despesas = despesaService.listByCnes("%porche%");
		assertThat(despesas).isNotNull().hasSize(2);
	}

	@Test
	@UsingDataSet("despesas.yml")
	public void shouldPaginateDespesas() {
		Filter<Despesa> despesaFilter = new Filter<Despesa>().setFirst(0).setPageSize(1);
		List<Despesa> despesas = despesaService.paginate(despesaFilter);
		assertNotNull(despesas);
		assertEquals(despesas.size(), 1);
		assertEquals(despesas.get(0).getId(), new Integer(1));
		despesaFilter.setFirst(1);// get second database page
		despesas = despesaService.paginate(despesaFilter);
		assertNotNull(despesas);
		assertEquals(despesas.size(), 1);
		assertEquals(despesas.get(0).getId(), new Integer(2));
		despesaFilter.setFirst(0);
		despesaFilter.setPageSize(4);
		despesas = despesaService.paginate(despesaFilter);
		assertEquals(despesas.size(), 4);
	}

	@Test
	@UsingDataSet("despesas.yml")
	public void shouldPaginateAndSortDespesas() {
		Filter<Despesa> despesaFilter = new Filter<Despesa>().setFirst(0).setPageSize(4).setSortField("cnes").setAdminSort(AdminSort.DESCENDING);
		List<Despesa> despesas = despesaService.paginate(despesaFilter);
		assertThat(despesas).isNotNull().hasSize(4);
		assertTrue(despesas.get(0).getCnes().equals("Porche274"));
		assertTrue(despesas.get(3).getCnes().equals("Ferrari"));
	}

	@Test
	@UsingDataSet("despesas.yml")
	public void shouldPaginateDespesasByCnes() {
		Despesa despesaExample = new Despesa().cnes("Ferrari");
		Filter<Despesa> despesaFilter = new Filter<Despesa>().setFirst(0).setPageSize(4).setEntity(despesaExample);
		List<Despesa> despesas = despesaService.paginate(despesaFilter);
		assertThat(despesas).isNotNull().hasSize(1).extracting("cnes").contains("Ferrari");
	}

	@Test
	@UsingDataSet("despesas.yml")
	public void shouldPaginateDespesasByValor() {
		Despesa despesaExample = new Despesa().valor(12999.0);
		Filter<Despesa> despesaFilter = new Filter<Despesa>().setFirst(0).setPageSize(2).setEntity(despesaExample);
		List<Despesa> despesas = despesaService.paginate(despesaFilter);
		assertThat(despesas).isNotNull().hasSize(1).extracting("cnes").contains("Mustang");
	}

	@Test
	@UsingDataSet("despesas.yml")
	public void shouldPaginateDespesasByIdInParam() {
		Filter<Despesa> despesaFilter = new Filter<Despesa>().setFirst(0).setPageSize(2).addParam("id", 1);
		List<Despesa> despesas = despesaService.paginate(despesaFilter);
		assertThat(despesas).isNotNull().hasSize(1).extracting("id").contains(1);
	}

	@Test
	@UsingDataSet("despesas.yml")
	@Transactional(value = TransactionMode.DISABLED)
	public void shouldListDespesasByValor() {
		List<Despesa> despesas = despesaService.criteria().between(Despesa_.valor, 1000D, 2450.9).orderAsc(Despesa_.valor).getResultList();
		// ferrari and porche
		assertThat(despesas).isNotNull().hasSize(2).extracting("cnes").contains("Porche", "Ferrari");
	}

	@Test
	@UsingDataSet("despesas.yml")
	public void shouldGetDespesaCness() {
		List<String> cness = despesaService.getCness("po");
		// porche and Porche274
		assertThat(cness).isNotNull().hasSize(2).contains("Porche", "Porche274");
	}

	@Test
	@UsingDataSet("despesas.yml")
	public void shoulListDespesasUsingCrudUtility() {
		assertEquals(4, crudService.count().intValue());
		long count = crudService.count(crudService.criteria().likeIgnoreCase(Despesa_.cnes, "%porche%").gtOrEq(Despesa_.valor, 10000D));
		assertEquals(1, count);

	}

	@Test
	@UsingDataSet("despesas.yml")
	public void shouldFindDespesasByExample() {
		Despesa despesaExample = new Despesa().cnes("Ferrari");
		List<Despesa> despesas = crudService.example(despesaExample, Despesa_.cnes).getResultList();
		assertThat(despesas).isNotNull().hasSize(1).extracting("cnes").contains("Ferrari");

		despesaExample = new Despesa().cnes("porche").competencia("%avenger");
		despesas = crudService.exampleLike(despesaExample, Despesa_.competencia, Despesa_.cnes).getResultList();

		assertThat(despesas).isNotNull().hasSize(1).extracting("competencia").contains("porche avenger");

	}

	@Test
	@UsingDataSet("despesas.yml")
	public void shoulGetTotalValorByCnes() {
		assertEquals((Double) 20380.53, despesaService.getTotalValorByCnes(new Despesa().cnes("%porche%")));
	}
}
